Ext.define('expand.editor.WangEditor', {
    extend: 'Ext.form.field.Base',
    alias: 'widget.wangEditor',
    initComponent: function () {
        var me = this;
        var E = window.wangEditor;
        me.editor = new E('#' + me.id);
        me.editor.customConfig.uploadImgServer = me.uploadImgServer;
        //用base64存图片
        me.editor.customConfig.uploadImgShowBase64 = false;
        me.editor.customConfig.uploadFileName = me.uploadFileName;
        me.editor.customConfig.menus = me.menusConfig;
        me.editor.customConfig.customUploadImg = function (files, insert) {
            // files 是 input 中选中的文件列表
            // insert 是获取图片 url 后，插入到编辑器的方法
            console.log('自定义上传');
            // 上传代码返回结果之后，将图片插入到编辑器中
            insert(imgUrl)
        }
        me.editor.customConfig.customAlert = function (info) {
            // info 是需要提示的内容
            EU.toastError(info);
        }
        //自定义编辑器菜单功能
        this.callParent(arguments);
    },
    menusConfig: [
        'head',  // 标题
        'bold',  // 粗体
        'fontSize',  // 字号
        'fontName',  // 字体
        'italic',  // 斜体
        'underline',  // 下划线
        'strikeThrough',  // 删除线
        'foreColor',  // 文字颜色
        'backColor',  // 背景颜色
        'link',  // 插入链接
        'list',  // 列表
        'justify',  // 对齐方式
        'quote',  // 引用
        // 'emoticon',  // 表情
        'image',  // 插入图片
        'table',  // 表格
        // 'video',  // 插入视频
        'code',  // 插入代码
        'undo',  // 撤销
        'redo'  // 重复
    ],
    uploadImgServer: null,
    uploadFileName: null,
    listeners: {
        afterrender: function () {
            var me = this;
            me.editor.create();
        }
    }
});
