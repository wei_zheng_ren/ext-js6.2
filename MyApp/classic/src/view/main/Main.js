/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('myapp.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',

        'myapp.view.main.MainController',
        'myapp.view.main.MainModel',
        'myapp.view.main.List',
        'myapp.view.Tree.CheckTree',
        'myapp.view.baobiao.Baobiao',
        'myapp.view.panelandtabl.TestPanel',
        'myapp.view.baobiao.StatementPanel',
        'myapp.view.questionnaire.Main',
        'myapp.view.questionnaire.SampleView',
        'myapp.view.questionnaire.Windowszz'

    ],

    controller: 'main',
    viewModel: 'main',

    ui: 'navigation',

    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },
        iconCls: 'fa-th-list'
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding: 20,
        tabConfig: {
            plugins: 'responsive',
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },

    items: [{
        title: 'Home',
        iconCls: 'fa-home',
        // The following grid shares a store with the classic version's grid as well!
        items: [{
            xtype: 'mainlist'
        }]
    }, {
        title: 'Users',
        iconCls: 'fa-user',
        items:[
            {
                xtype: 'check-tree'
            }
        ]
    }, {
        title: '测试',
        iconCls: 'fa-users',
        items:[
            {
                xtype:'SumBaoBiao'
            }
        ]
    }, {
        title: '问卷',
        iconCls: 'fa-users',
        items:[
            {
                xtype:'questionnaire-main'
            }
        ]
    }, {
        title: '示例',
        iconCls: 'fa-users',
        items:[
            {
                xtype:'questionnaire-SampleView'
            }
        ]
    },{
        title: 'Settings',
        iconCls: 'fa-cog',
       items:[
           {
               xtype:'windowszz'
           }
       ]
    }]
});
