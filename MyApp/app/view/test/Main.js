Ext.define('myapp.view.test.Main', {
    extend: 'Ext.panel.Panel',
    xtype: 'panel-header-position',


    width: 600,
    layout: 'column',
    viewModel: true,

    defaults: {
        bodyPadding: 10,
        height: 300,
        scrollable: true
    },

    initComponent: function() {

        this.bodyStyle = "background: transparent";
        this.title='测试'
        this.tbar = [
            {
                xtype: 'label',
                text: 'Header Position:'
            },
            {
                xtype: 'segmentedbutton',
                reference: 'positionBtn',
                value: 'top',
                defaultUI: 'default',
                items: [{
                    text: 'Top',
                    value: 'top'
                }, {
                    text: 'Right',
                    value: 'right'
                }, {
                    text: 'Bottom',
                    value: 'bottom'
                }, {
                    text: 'Left',
                    value: 'left'
                }]
            }
        ];

        this.items = [{
            columnWidth: 0.5,
            margin: "10 5 0 0",
            title: 'Panel',
            icon: null,
            glyph: 117,
            html: 'fasda',
            bind: {
                headerPosition: '{positionBtn.value}'
            }
        }, {
            columnWidth: 0.5,
            margin: "10 0 0 5",
            frame: true,
            title: 'Framed Panel',
            iconCls: 'x-fa fa-user',
            glyph: 117,
            html:'asd',
            bind: {
                headerPosition: '{positionBtn.value}'
            }
        }];

        this.callParent();
    }
});