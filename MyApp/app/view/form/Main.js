Ext.define('myapp.view.form.Main', {
    extend: 'Ext.panel.Panel',
    xtype: 'form-main',
    title:'表单',
    width: 600,
    border:true,
    height:500,
    layout: 'column',
    viewModel: true,
    items:[
        {
            xtype:'textfield',
            width:'98%',
            labelAlign : 'right',
            emptyText:'请输入姓名',
            regex:/^[0-9]+$/,
            regexText:"只能输入数字",
            allowBlank: false,
            blankText:'此处为必填项'
        }
    ]

});