Ext.define('myapp.view.questionnaire.Main', {
    extend: 'Ext.container.Container',
    xtype: 'questionnaire-main',
    controller:'questionnaire-main',
    requires: [
       'app.ux.UEditor'
    ],
    title:'问卷调查',
    width: '100%',
    border:true,
    height:900,
    ascrollable: true,
    layout: {
        type: 'hbox',
        pack: 'start',
        align: 'stretch'
    },
    bodyPadding: 10,
    viewModel: {
        data:{
            content:'',
            records:null
        }
    },
    defaults: {
        frame: true,
        // bodyPadding: 10
    },
    items:[
        {
            title: '编辑',
            flex: 1,
            xtype:'panel',
            height:'100%',
            tbar:[
                '->',
                {
                    xtype:'button',
                    text:'示例与帮助',
                    handler:'onSample'
                }
            ],
            // margin: '0 10 0 0',
            items:[
                {
                    xtype: 'textarea',
                    width:'100%',
                    height:'100%',
                    hideLabel: true,
                    bind:{
                        value:'{content}'
                    },
                    name: 'content',
                    flex: 1,
                    listeners:{
                        change:'onEdit'
                    }
                }
            ]
        },
        {
            title: '预览',
            flex: 1,
            xtype:'form',
            tbar:[
                '->',
                {
                    xtype:'button',
                    text:'保存',
                    handler:function(){
                        Ext.create('Ext.window.Window', {
                             // 引入包装好的编辑器\
                            title:'测试富文本编辑器',
                            layout: 'fit',
                            width: '50%',
                            height:600,
                            autoScroll: true,
                            constrain: true,
                            items:[
                                {
                                    xtype: 'ueditor',
                                    autoScroll: true,
                                    height:600
                                }
                            ]
                        }).show();
                    }
                }
            ],
            padding:'0 0 0 50',
            autoScroll: true,
            reference:'dynaform'
        },
    ]

});
