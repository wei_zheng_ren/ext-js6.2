Ext.define('myapp.view.questionnaire.SampleView', {
    extend: 'Ext.container.Container',
    xtype: 'questionnaire-SampleView',
    // controller:'questionnaire-SampleView',
    title: '示例&帮助',
    width: '100%',
    items: [
        {
            /*====================================================================
             * Individual checkbox/radio examples
             *====================================================================*/

            // Using checkbox/radio groups will generally be more convenient and flexible than
            // using individual checkbox and radio controls, but this shows that you can
            // certainly do so if you only have a single control at a time.
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'fieldset',
                flex: 1,
                title: '单选题',
                height: 250,
                // checkboxToggle: true,
                defaultType: 'checkbox', // each item will be a checkbox
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    hideEmptyLabel: false
                },
                items: [
                    {
                        xtype: 'component',
                        anchor: '100%',
                        html: [
                            '<p>1、问卷网模板库有多少免费模板[单选题]<br>' +
                            '10万<br>' +
                            '20万<br>' +
                            '答案解析<br>' +
                            '问卷网精选了20多万套模板供用户免费使用</p>'
                        ]
                    }
                ]
            }, {
                xtype: 'component',
                width: 10
            }, {
                xtype: 'fieldset',
                flex: 1,
                height: 250,
                title: '预览示例',
                // checkboxToggle: true,
                defaultType: 'radio', // each item will be a radio button
                layout: 'anchor',
                defaults: {
                    anchor: '100%',
                    hideEmptyLabel: false
                },

                items: [
                    {
                        xtype: 'image',
                        src:'resources/images/document.png',
                        width:200,
                        height:200,
                    }
                ]
            }]
        }
    ]

});
