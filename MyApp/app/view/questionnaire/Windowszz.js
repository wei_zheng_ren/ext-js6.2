Ext.define('myapp.view.questionnaire.Windowszz', {
    extend: 'Ext.form.field.HtmlEditor',
    xtype: 'windowszz',
    enableImage:true,
    addImage : function() {
        var editor = this;
        var imgform = new Ext.FormPanel({
            region : 'center',
            labelWidth : 55,
            frame : true,
            bodyStyle : 'padding:5px 5px 0',
            autoScroll : true,
            border : false,
            fileUpload : true,
            items : [{
                xtype : 'textfield',
                fieldLabel : '选择文件',
                name : 'userfile',
                inputType : 'file',
                allowBlank : false,
                blankText : '文件不能为空',
                height : 25,
                anchor : '90%'
            }],
            buttons : [{
                text : '上传',
                type : 'submit',
                handler : function() {
                    if (!imgform.form.isValid()) {return;}
                    imgform.form.submit({
                        waitMsg : '正在上传',
                        url : 'Default.aspx',
                        success : function(form, action) {
                            var element = document.createElement("img");
                            element.src = action.result.fileURL;
                            if (Ext.isIE) {
                                editor.insertAtCursor(element.outerHTML);
                            } else {
                                var selection = editor.win.getSelection();
                                if (!selection.isCollapsed) {
                                    selection.deleteFromDocument();
                                }
                                selection.getRangeAt(0).insertNode(element);
                            }
                            win.hide();
                        },
                        failure : function(form, action) {
                            form.reset();
                            if (action.failureType == Ext.form.Action.SERVER_INVALID)
                                Ext.MessageBox.alert('警告',
                                    action.result.errors.msg);
                        }
                    });
                }
            }, {
                text : '关闭',
                type : 'submit',
                handler : function() {
                    win.close(this);
                }
            }]
        })

        var win = new Ext.Window({
            title : "上传图片",
            width : 300,
            height : 200,
            modal : true,
            border : false,
            iconCls : "picture.png",
            layout : "fit",
            items : imgform

        });
        win.show();
    },
    createToolbar : function(editor) {
        var me=this;


    },
    getToolbarCfg:function () {
        var me = this,
            items = [], i,
            tipsEnabled = Ext.quickTipsActive && Ext.tip.QuickTipManager.isEnabled(),
            baseCSSPrefix = Ext.baseCSSPrefix;
        function btn(id, toggle, handler){
            return Ext.merge({
                itemId: id,
                cls: baseCSSPrefix + 'btn-icon',
                iconCls: baseCSSPrefix + 'edit-'+id,
                enableToggle:toggle !== false,
                scope: me,
                handler:handler||me.relayBtnCmd,
                clickEvent: 'mousedown',
                tooltip: tipsEnabled ? me.buttonTips[id] : undef,
                overflowText: me.buttonTips[id].title || undef,
                tabIndex: -1
            }, me.buttonDefaults);
        }
        if (me.enableImage) {
            items.push(this.addImage)
        }
    },

});
