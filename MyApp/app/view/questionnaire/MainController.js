/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('myapp.view.questionnaire.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.questionnaire-main',

    init: function () {

    },
    onEdit: function (textArea, newValue, oldValue, eOpts) {
        var dynaform = this.lookupReference('dynaform');
        dynaform.removeAll();
        var contentList = newValue.split('\n');
        var list = [];
        var j = 0;
        var records = [];
        var param={
            orgCode:"",
            title:"",
            type:"",
            content:'',
            operator:"",
            lifeTime:null,
            analysis:[],
            record:[]
        };
        for (var i = 1; i < contentList.length; i++) {
            if (contentList[i] == '' || contentList[i] == null) {
                //截取不是空字符的数据
                if (contentList[j] == '') {
                    j = i + 1;
                    continue
                }
                var newContent = contentList.slice(j, i)
                list.push(newContent);
                // list.push([""]);
                j = i + 1;
            }
            if (i == contentList.length - 1) {
                var newContent = contentList.slice(j);
                list.push(newContent);
            }
        }
        var str = '';
        if (list.length == 0) {
            return
        }
        for (var i = 1; i < list[0].length; i++) {
            str += list[0][i];
        }
        //添加标题
        dynaform.add({
            xtype: 'component',
            anchor: '100%',
            html: [
                '<h3>' + list[0][0] + '</h3>',
                '<p>' + str + '</p>'
            ]
        })
        param.content=str;
        param.title=list[0][0];
        for (var i = 0; i < list.length; i++) {
            this.getListContent(list[i], dynaform, i, param)
        }

        this.view.getViewModel().getData().records=param;
    },
    /**
     * 点击"示例与帮助"按钮
     */
    onSample:function(){
        
    },
    /**
     * 判断题型
     * @param {*} data 
     * @param {*} form 
     * @param {*} index 
     */
    getListContent: function (data, form, index, param) {
        if (data.length == 0) {
            return
        }
        var list = data;
        if (list.length == 1 && list[0] == '') {
            return
        }
        // var records = this.view.getViewModel().getData().records;

        if (list[0].indexOf('单选题') >= 0) {
            var record = this.getType1(list, index, 1);
            param.record.push(record);
            this.formAddRadiogroup(form, record);

        }
        if (list[0].indexOf('多选题') >= 0) {
            var record = this.getType1(list, index, 2);
            param.record.push(record);
            this.formAddFieldcontainer(form, record);
        }
        if (list[0].indexOf('判断题') >= 0) {
            var record = this.getType3(list, index, 3);
            param.record.push(record);
            this.formAddRadiogroup(form, record);
        }
        if (list[0].indexOf('填空题') >= 0) {
            var record = this.getType4(list, index, 4);
            param.record.push(record);
            form.add({
                xtype: 'component',
                anchor: '100%',
                html: [
                    '<p>' + list[0] + '</p>'
                ]
            });
        }
        if(list[0].indexOf('得分解析') >= 0){
            //获取解析列表
            var analysis = list.slice(1, list.length);
            for(var i=0;i<analysis.length;i++){
                var analysi=this.analysisScore(analysis[i],i+1);
                param.analysis.push(analysi)
            //    param.analysi.push();
            }
        }
    },
    /**
     * 解析得分说明列表
     * @param {*} str 
     * @param {*} index 
     */
    analysisScore:function(str,index){
        //去掉中括号
        str = str.substring(1, str.length - 1);
        var list = str.split(',')
        var analysi = {
            serialNumber: index,
            rule: list[0],
            explain: list[1]
        }
        return analysi;
    },
    /**
     * 解析填空题
     * @param {*} data 
     * @param {*} form 
     * @param {*} index 
     */
    getType4: function (list, index, type) {
        var content = list[0];
        var record = {
            content: content,
            serialNumber: index,
            type: type,
            rule: null,
            detail: null,
            analysis: []
        };
        //获取规则下标
        var xiabiao = null;
        for (var i = 0; i < list.length; i++) {
            if (list[i].indexOf('规则') >= 0) {
                xiabiao = i;
            }
        }
        //获取解析列表
        var analysis = list.slice(2, xiabiao);
        var me = this;
        for (var i = 0; i < analysis.length; i++) {
            record.analysis.push(me.analysisJudge(analysis[i], i + 1));
        }
        //获取规则表达式
        if (xiabiao != null && xiabiao != '') {
            record.rule = list[xiabiao + 1];
        }
        return record;
    },
    /**
     * 解析填空题的答案解析内容
     * @param {*} str 
     */
    analysisJudge: function (str, index) {
        //去掉中括号
        str = str.substring(1, str.length - 1);
        var list = str.split(',')
        var analysi = {
            serialNumber: index,
            rule: list[0],
            explain: list[1],
            score: list[2]
        }
        return analysi;
    },
    /**
     * 解析判断题
     * @param {*} list 
     * @param {*} indexm 
     * @param {*} type 
     */
    getType3: function (list, index, type) {
        var record = {
            content: list[0],
            serialNumber: index,
            type: type,
            rule: null,
            detail: [
                {
                    content: "正确",
                    serialNumber: 1
                },
                {
                    content: "错误",
                    serialNumber: 2
                }
            ],
            analysis: []
        };
        //获取答案解析列表
        var analysis = list.slice(2, list.length);//去题目,去答案解析字段
        for (var i = 0; i < analysis.length; i++) {
            var analysisInfo = {
                serialNumber: i + 1,
                rule: null,
                explain: analysis[i],
                score: 10
            }
            record.analysis.push(analysisInfo);
        }
        return record;

    },
    /**
     * 解析单选题/多选题
     * @param {*} list 
     * @param {*} index 
     */
    getType1: function (list, index, type) {
        var record = {
            content: list[0],
            serialNumber: index,
            type: type,
            rule: null,
            detail: [],
            analysis: []
        };
        var newList = list.slice(1, list.length);

        var xiabiao = 0;
        for (var i = 0; i < newList.length; i++) {
            if (list[i].indexOf('答案解析') >= 0) {
                xiabiao = i;
            }
        }
        //获取选项列表
        var detail = newList.slice(0, xiabiao - 1);
        for (var i = 0; i < detail.length; i++) {
            var detailInfo = {
                content: detail[i],
                serialNumber: i + 1
            }
            record.detail.push(detailInfo);
        }
        //获取答案解析列表
        var analysis = newList.slice(xiabiao, newList.length);
        for (var i = 0; i < analysis.length; i++) {
            var analysisInfo = {
                serialNumber: i + 1,
                rule: null,
                explain: analysis[i],
                score: 10
            }
            record.analysis.push(analysisInfo);
        }
        return record;
    },
    /**
     * 添加单选框
     * @param {*} from 
     * @param {*} record 
     */
    formAddRadiogroup: function (from, record) {
        var list = record.detail;
        var itemlist = [];
        for (var i = 0; i < list.length; i++) {
            var ad = { boxLabel: list[i].content, name: record.content, inputValue: list[i].serialNumber };
            itemlist.push(ad);
        }
        from.add({
            xtype: 'radiogroup',
            labelAlign: 'top',
            layout: {
                type: 'vbox',
                align: 'left'
            },
            fieldLabel: record.content,
            columns: 2,
            vertical: true,
            items: itemlist
        });
    },
    /**
     * 添加多选框
     * @param {*} from 
     * @param {*} record 
     */
    formAddFieldcontainer: function (form, record) {
        var list = record.detail;
        var itemlist = [];
        for (var i = 0; i < list.length; i++) {
            var ad = {
                boxLabel: list[i].content,
                name: record.content,
                inputValue: list[i].serialNumber
            };
            itemlist.push(ad);
        }
        form.add({
            labelAlign: 'top',
            layout: {
                type: 'vbox',
                align: 'left'
            },
            fieldLabel: record.content,
            vertical: true,
            xtype: 'fieldcontainer',
            defaultType: 'checkboxfield',
            items: itemlist
        });
    }
});

