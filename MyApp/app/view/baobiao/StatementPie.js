/**
 * 医院错误数柱形图
 */
Ext.define('myapp.view.baobiao.StatementPie', {
    extend: 'Ext.panel.Panel',
    xtype: 'system-quality-gauging-StatementPie',
    padding:20,
    flipXY: true,
    initComponent:function () {
        var me=this;
        var xFieldName=me.params.xField;
        var yFieldData=me.params.yField;
        var url=me.params.url;
        var text=me.params.text;
        var mystore=me.params.store;
        var rootProperty=me.params.rootProperty;
        Ext.apply(me, {
            items:[
                {
                    xtype: "polar",
                    width: '100%',
                    height: 450,
                    plugins: {
                        ptype: 'chartitemevents',
                        moveEvents: true
                    },
                    insetPadding: 10,
                    innerPadding: 10,
                    interactions: ['rotate', 'itemhighlight'],
                    //store:me.myStore,
                    bind: {
                        store: mystore
                    },
                    animate: true,
                    legend: {
                        docked: 'right'
                    },
                    sprites: [{
                        type: 'text',
                        text: text,
                        fontSize: 18,
                        width: 100,
                        height: 30,
                        x: 0, // the sprite x position
                        y: 20  // the sprite y position
                    }],
                    series: [{
                        type: 'pie',
                        autoScroll: true,
                        angleField: yFieldData,
                        //radiusField:yFieldData,
                        label: {
                            field: xFieldName,
                            display: 'rotate',
                            font: '10px Arial',
                            renderer: function (text, sprite, config, rendererData, index) {
                                var Data = rendererData.store.data.items;
                                var thisdata = Data[index];
                                var percent = ((parseFloat(thisdata.get(yFieldData) / rendererData.store.sum(yFieldData)) * 100.0).toFixed(2))
                                return  percent + "%";
                            }
                        },
                        triggerAfterDraw: true,
                        donut: true,
                        showInLegend: true,
                        tips: {
                            trackMouse: true,
                            renderer: function (storeItem, item) {
                                var total = 0;
                                this.getStore().each(function (rec) {
                                    total += rec.get(yFieldData);
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.get(xFieldName) + ":" + item.get(yFieldData))
                            }
                        },
                        highlightCfg: {
                            margin: 10,
                            fillOpacity: .7
                        },
                        highlight: {
                            segment: {
                                margin: 20
                            }
                        }
                    }]

                }
            ]
        });
        this.callParent();
    }
});
