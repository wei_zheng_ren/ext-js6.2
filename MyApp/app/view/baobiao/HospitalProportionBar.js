/**
 * 医院错误数柱形图
 */
Ext.define('myapp.view.baobiao.HospitalProportionBar', {
    extend: 'Ext.panel.Panel',
    xtype: 'system-quality-gauging-hospitalProportionBar',
    padding:20,
    insetPadding: 30,
    flipXY: true,
    initComponent:function () {
        var me=this;
        var xFieldName=me.params.xField;
        var yFieldData=me.params.yField;
        var url=me.params.url;
        var text=me.params.text;
        var rootProperty=me.params.rootProperty;
        Ext.apply(me, {
            items:[
                {
                    xtype: 'cartesian',
                    reference: 'chart',
                    width: '100%',
                    height: 700,
                    minHeight:500,
                    maxHeight:850,
                    autoScroll: true,
                    insetPadding: 40,
                    flipXY: true,
                    //store:me.myStore,
                    bind:{
                        store:'{'+rootProperty+'}'
                    },
                    animation: {
                        easing: 'easeOut',
                        duration: 500
                    },
                    axes: [{
                        type: 'numeric',
                        position: 'bottom',
                        fields: yFieldData,
                        grid: true,
                        //maximum: 40000000,
                        majorTickSteps: 10,
                        title: '错误数据',
                        renderer: function (axis, label, layoutContext) {
                            return Ext.util.Format.number(layoutContext.renderer(label) , '0,000');
                        }
                    }, {
                        type: 'category',
                        position: 'left',
                        fields: xFieldName,
                        grid: true
                    }],
                    series: [{
                        type: 'bar',
                        xField: xFieldName,
                        yField: yFieldData,
                        style: {
                            opacity: 0.80,
                            minGapWidth: 3
                        },
                        highlightCfg: {
                            strokeStyle: 'black',
                            radius: 10
                        },
                        label: {
                            field: yFieldData,
                            display: 'insideEnd',
                            renderer: function (value) {
                                return Ext.util.Format.number(value , '0,000');
                            }
                        },
                        renderer:function(sprite, config, rendererData, index){
                            return config;
                        },
                        tooltip: {
                            trackMouse: true,
                            renderer:  function(tooltip, record, item) {
                                var formatString = '0,000';
                                tooltip.setHtml(record.get(xFieldName) + ': ' +
                                    Ext.util.Format.number(record.get(yFieldData), formatString));
                            }
                        }
                    }],
                    sprites: [{
                        type: 'text',
                        text: text,
                        fontSize: 18,
                        width: 100,
                        height: 30,
                        x: 0, // the sprite x position
                        y: 20  // the sprite y position
                    }],
                    listeners: {
                        redraw :function (aa,e) {
                            var length=this.getStore().data.length;
                            if(length>0){
                               aa.setHeight(length*15+30);
                            }
                        }
                    },
                }
            ]
        });
        this.callParent();
    }
});
