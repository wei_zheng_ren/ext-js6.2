Ext.define('app.view.system.quality.gauging.BusinessProportion', {
    extend: 'Ext.panel.Panel',
    xtype:'BusinessProportion',
    title:"业务错误数据",
    controller:'system-quality-gauging-StatementPanelController',
    layout :'fit',
    autoScroll: true,
    width:'100%',
    collapsible: true,
    viewModel:{
        stores:{
            zqx:{
                fields: ['tableName', 'Errdata'],
                autoLoad: true,
                sortOnLoad: true,
                proxy: {
                    type: 'ajax',
                    url : '/dataRepotForms/getBusinessProportion',
                    reader: {
                        type: 'json',
                        rootProperty: 'zqx'
                    }
                },
                listeners: {
                    beforeload: 'onBeforeLoad'
                }
            },
            wzx:{
                fields: ['tableName', 'Errdata'],
                autoLoad: true,
                sortOnLoad: true,
                proxy: {
                    type: 'ajax',
                    url : '/dataRepotForms/getBusinessProportion',
                    reader: {
                        type: 'json',
                        rootProperty: 'wzx'
                    }
                },
                listeners: {
                    beforeload: 'onBeforeLoad'
                }
            },
            jsx:{
                fields: ['tableName', 'Errdata'],
                autoLoad: true,
                sortOnLoad: true,
                proxy: {
                    type: 'ajax',
                    url : '/dataRepotForms/getBusinessProportion',
                    reader: {
                        type: 'json',
                        rootProperty: 'jsx'
                    }
                },
                listeners: {
                    beforeload: 'onBeforeLoad'
                }
            },
            sxx:{
                fields: ['tableName', 'Errdata'],
                autoLoad: true,
                sortOnLoad: true,
                proxy: {
                    type: 'ajax',
                    url : '/dataRepotForms/getBusinessProportion',
                    reader: {
                        type: 'json',
                        rootProperty: 'sxx'
                    }
                },
                listeners: {
                    beforeload: 'onBeforeLoad'
                }
            }
        }
    },
    height:900,
    items:[
        {
            xtype:'panel',
            width:'100%',
            height:'100%',
            layout: 'column',
            autoScroll: true,
            items: [
                {
                    xtype:"polar",
                    //padding:'10 0 10 0',
                    //title:'准确性错误数扇形图',
                    padding:5,
                    height: 450,
                    plugins: {
                        ptype: 'chartitemevents',
                        moveEvents: true
                    },
                    insetPadding: 10,
                    innerPadding: 10,
                    interactions: ['rotate', 'itemhighlight'],
                    bind:{
                        store:'{zqx}'
                    },
                    animate:true,
                    legend: {
                        position: 'right'
                    },
                    sprites: [{
                        type: 'text',
                        text: '准确性错误数占比',
                        fontSize: 18,
                        width: 100,
                        height: 30,
                        x: 0, // the sprite x position
                        y: 20  // the sprite y position
                    }],
                    series:[{
                        type: 'pie',
                        autoScroll: true,
                        angleField: 'Errdata',
                        //radiusField:'data',
                        label: {
                            field: 'tableName',
                            display: 'rotate',
                            font:10,
                            renderer: function (text, sprite, config, rendererData, index) {
                                var Data = rendererData.store.data.items;
                                var thisdata = Data[index];
                                var percent = ((parseFloat(thisdata.get('Errdata') / rendererData.store.sum('Errdata')) * 100.0).toFixed(2))
                                return text + "-"+percent+"%";
                            }
                        },
                        triggerAfterDraw:true,
                        donut:true,
                        // colors: [ "#a61120","#94ae0a"],
                        showInLegend: true,
                        // label:{
                        //     display:"name",
                        //     contrast:true,
                        // },
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+":"+item.data.Errdata)
                            }
                        },
                        highlightCfg: {
                            margin: 10,
                            fillOpacity: .7
                        },
                        highlight:{
                            segment:{
                                margin:20
                            }
                        },
                        listeners:{
                            itemclick:function( series, item, event, eOpts){
                                // var rec=this.getStore().getAt(item.index);
                            }
                        }

                    }],
                    columnWidth:0.5
                },
                {
                    xtype: 'cartesian',
                    //title:'准确性错误数据柱形图',
                    padding:5,
                    height: 450,
                    autoScroll: true,
                    insetPadding: 30,
                    innerPadding: 5,
                    bind:{
                        store:'{zqx}'
                    },
                    axes: [{
                        type: 'numeric',
                        position: 'left',
                        title: {
                            text: '错误数',
                            fontSize: 15
                        },
                        fields: 'Errdata'
                    }, {
                        type: 'category',
                        position: 'bottom',
                        title: {
                            text: '业务字段',
                            fontSize: 15
                        },
                        fields: 'tableName'
                    }],
                    sprites: [{
                        type: 'text',
                        text: '准确性错误数统计',
                        fontSize: 18,
                        width: 100,
                        height: 30,
                        x: 0, // the sprite x position
                        y: 20  // the sprite y position
                    }],
                    series: {
                        type: 'bar',
                        subStyle: {
                            fill: ['#388FAD'],
                            stroke: '#1F6D91'
                        },
                        xField: 'tableName',
                        yField: 'Errdata',
                        highlight: {
                            strokeStyle: 'black',
                            fillStyle: 'gold'
                        },
                        label: {
                            field: 'Errdata',
                            display: 'insideEnd',
                            renderer:function (value) {
                                return value;
                            }
                        },
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+':'+item.data.Errdata)
                            }
                        }
                    },
                    columnWidth:0.5
                },
                {
                    xtype:"polar",
                    //padding:'10 0 10 0',
                    //title:'完整性错误数扇形图',
                    padding:5,
                    height: 450,
                    plugins: {
                        ptype: 'chartitemevents',
                        moveEvents: true
                    },
                    insetPadding: 10,
                    innerPadding: 10,
                    interactions: ['rotate', 'itemhighlight'],
                    bind:{
                        store:'{wzx}'
                    },
                    animate:true,
                    legend: {
                        position: 'right'
                    },
                    sprites: [{
                        type: 'text',
                        text: '完整性错误数占比',
                        fontSize: 18,
                        width: 100,
                        height: 30,
                        x: 0, // the sprite x position
                        y: 20  // the sprite y position
                    }],
                    series:[{
                        type: 'pie',
                        autoScroll: true,
                        angleField: 'Errdata',
                        //radiusField:'data',
                        label: {
                            field: 'tableName',
                            display: 'rotate',
                            font:10,
                            renderer: function (text, sprite, config, rendererData, index) {
                                var Data = rendererData.store.data.items;
                                var thisdata = Data[index];
                                var percent = ((parseFloat(thisdata.get('Errdata') / rendererData.store.sum('Errdata')) * 100.0).toFixed(2))
                                return text + "-"+percent+"%";
                            }
                        },
                        triggerAfterDraw:true,
                        donut:true,
                        // colors: [ "#a61120","#94ae0a"],
                        showInLegend: true,
                        // label:{
                        //     display:"name",
                        //     contrast:true,
                        // },
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+":"+item.data.Errdata)
                            }
                        },
                        highlightCfg: {
                            margin: 10,
                            fillOpacity: .7
                        },
                        highlight:{
                            segment:{
                                margin:20
                            }
                        },
                        listeners:{
                            itemclick:function( series, item, event, eOpts){
                                // var rec=this.getStore().getAt(item.index);
                            }
                        }

                    }],
                    columnWidth:0.5
                },
                {
                    xtype: 'cartesian',
                    //title:'完整性错误数据柱形图',
                    padding:5,
                    height: 450,
                    insetPadding: 30,
                    innerPadding: 5,
                    autoScroll: true,
                    bind:{
                        store:'{wzx}'
                    },
                    axes: [{
                        type: 'numeric',
                        position: 'left',
                        title: {
                            text: '错误数',
                            fontSize: 15
                        },
                        fields: 'Errdata'
                    }, {
                        type: 'category',
                        position: 'bottom',
                        title: {
                            text: '业务字段',
                            fontSize: 15
                        },
                        fields: 'tableName'
                    }],
                    sprites: [{
                        type: 'text',
                        text: '完整性错误数统计',
                        fontSize: 18,
                        width: 100,
                        height: 30,
                        x: 0, // the sprite x position
                        y: 20  // the sprite y position
                    }],
                    series: {
                        type: 'bar',
                        subStyle: {
                            fill: ['#388FAD'],
                            stroke: '#1F6D91'
                        },
                        xField: 'tableName',
                        yField: 'Errdata',
                        highlight: {
                            strokeStyle: 'black',
                            fillStyle: 'gold'
                        },
                        label: {
                            field: 'Errdata',
                            display: 'insideEnd',
                            renderer:function (value) {

                                return value;
                            }
                        },
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+':'+item.data.Errdata)
                            }
                        }
                    },
                    columnWidth:0.5
                },
                {
                    xtype:"polar",
                    //padding:'10 0 10 0',
                    //title:'及时性错误数扇形图',
                    padding:5,
                    height: 450,
                    plugins: {
                        ptype: 'chartitemevents',
                        moveEvents: true
                    },
                    insetPadding:10,
                    innerPadding:10,
                    interactions: ['rotate', 'itemhighlight'],
                    bind:{
                        store:'{jsx}'
                    },
                    animate:true,
                    legend: {
                        position: 'right'
                    },
                    sprites: [{
                        type: 'text',
                        text: '及时性错误数占比',
                        fontSize: 18,
                        width: 100,
                        height: 30,
                        x: 0, // the sprite x position
                        y: 20  // the sprite y position
                    }],
                    series:[{
                        type: 'pie',
                        autoScroll: true,
                        angleField: 'Errdata',
                        //radiusField:'data',
                        label: {
                            field: 'tableName',
                            display: 'rotate',
                            font:10,
                            renderer: function (text, sprite, config, rendererData, index) {
                                var Data = rendererData.store.data.items;
                                var thisdata = Data[index];
                                var percent = ((parseFloat(thisdata.get('Errdata') / rendererData.store.sum('Errdata')) * 100.0).toFixed(2))
                                return text + "-"+percent+"%";
                            }
                        },
                        triggerAfterDraw:true,
                        donut:true,
                        // colors: [ "#a61120","#94ae0a"],
                        showInLegend: true,
                        // label:{
                        //     display:"name",
                        //     contrast:true,
                        // },
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+":"+item.data.Errdata)
                            }
                        },
                        highlightCfg: {
                            margin: 10,
                            fillOpacity: .7
                        },
                        highlight:{
                            segment:{
                                margin:20
                            }
                        },
                        listeners:{
                            itemclick:function( series, item, event, eOpts){
                                // var rec=this.getStore().getAt(item.index);
                            }
                        }

                    }],
                    columnWidth:0.5
                },
                {
                    xtype: 'cartesian',
                    //title:'及时错误数据柱形图',
                    padding:5,
                    height: 450,
                    insetPadding:30,
                    innerPadding:5,
                    autoScroll: true,
                    bind:{
                        store:'{jsx}'
                    },
                    axes: [{
                        type: 'numeric',
                        position: 'left',
                        title: {
                            text: '错误数',
                            fontSize: 15
                        },
                        fields: 'Errdata'
                    }, {
                        type: 'category',
                        position: 'bottom',
                        title: {
                            text: '业务字段',
                            fontSize: 15
                        },
                        fields: 'tableName'
                    }],
                    sprites: [{
                        type: 'text',
                        text: '及时性错误数统计',
                        fontSize: 18,
                        width: 100,
                        height: 30,
                        x: 0, // the sprite x position
                        y: 20  // the sprite y position
                    }],
                    series: {
                        type: 'bar',
                        subStyle: {
                            fill: ['#388FAD'],
                            stroke: '#1F6D91'
                        },
                        xField: 'tableName',
                        yField: 'Errdata',
                        highlight: {
                            strokeStyle: 'black',
                            fillStyle: 'gold'
                        },
                        label: {
                            field: 'Errdata',
                            display: 'insideEnd',
                            renderer:function (value) {

                                return value;
                            }
                        },
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+':'+item.data.Errdata)
                            }
                        }
                    },
                    columnWidth:0.5
                },
                {
                    xtype:"polar",
                    //padding:'10 0 10 0',
                    //title:'时效性错误数扇形图',
                    padding:5,
                    height: 450,
                    plugins: {
                        ptype: 'chartitemevents',
                        moveEvents: true
                    },
                    innerPadding: 10,
                    insetPadding:10,
                    interactions: ['rotate', 'itemhighlight'],
                    bind:{
                        store:'{sxx}'
                    },
                    animate:true,
                    legend: {
                        position: 'right'
                    },
                    sprites: [{
                        type: 'text',
                        text: '时效性错误数占比',
                        fontSize: 18,
                        width: 100,
                        height: 30,
                        x: 0, // the sprite x position
                        y: 20  // the sprite y position
                    }],
                    series:[{
                        type: 'pie',
                        autoScroll: true,
                        angleField: 'Errdata',
                        //radiusField:'data',
                        label: {
                            field: 'tableName',
                            display: 'rotate',
                            font:10,
                            renderer: function (text, sprite, config, rendererData, index) {
                                var Data = rendererData.store.data.items;
                                var thisdata = Data[index];
                                var percent = ((parseFloat(thisdata.get('Errdata') / rendererData.store.sum('Errdata')) * 100.0).toFixed(2))
                                return text + "-"+percent+"%";
                            }
                        },
                        triggerAfterDraw:true,
                        donut:true,
                        // colors: [ "#a61120","#94ae0a"],
                        showInLegend: true,
                        // label:{
                        //     display:"name",
                        //     contrast:true,
                        // },
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+":"+item.data.Errdata)
                            }
                        },
                        highlightCfg: {
                            margin: 10,
                            fillOpacity: .7
                        },
                        highlight:{
                            segment:{
                                margin:20
                            }
                        },
                        listeners:{
                            itemclick:function( series, item, event, eOpts){
                                // var rec=this.getStore().getAt(item.index);
                            }
                        }

                    }],
                    columnWidth:0.5
                },
                {
                    xtype: 'cartesian',
                    //title:'时效性错误数据柱形图',
                    padding:5,
                    insetPadding:30,
                    innerPadding:5,
                    height: 450,
                    autoScroll: true,
                    bind:{
                        store:'{sxx}'
                    },
                    axes: [{
                        type: 'numeric',
                        position: 'left',
                        title: {
                            text: '错误数',
                            fontSize: 15
                        },
                        fields: 'Errdata'
                    }, {
                        type: 'category',
                        position: 'bottom',
                        title: {
                            text: '业务字段',
                            fontSize: 15
                        },
                        fields: 'tableName'
                    }],
                    sprites: [{
                        type: 'text',
                        text: '时效性错误数统计',
                        fontSize: 18,
                        width: 100,
                        height: 30,
                        x: 0, // the sprite x position
                        y: 20  // the sprite y position
                    }],
                    series: {
                        type: 'bar',
                        subStyle: {
                            fill: ['#388FAD'],
                            stroke: '#1F6D91'
                        },
                        xField: 'tableName',
                        yField: 'Errdata',
                        highlight: {
                            strokeStyle: 'black',
                            fillStyle: 'gold'
                        },
                        label: {
                            field: 'Errdata',
                            display: 'insideEnd',
                            renderer:function (value) {

                                return value;
                            }
                        },
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+':'+item.data.Errdata)
                            }
                        }
                    },
                    columnWidth:0.5
                }
            ]
        }
    ]
});