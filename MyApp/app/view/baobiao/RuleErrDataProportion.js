Ext.define('myapp.view.baobiao.RuleErrDataProportion', {
    extend: 'Ext.panel.Panel',
    xtype: 'RuleErrDataProportion',
    title: "规则类型错误数总数",
    layout: 'fit',
    autoScroll: true,
    width: '100%',
    collapsible: true,
    height: 500,
    items: [
        {
            xtype:'panel',
            width:'100%',
            height:'100%',
            layout: 'column',
            autoScroll: true,
            items:[
                {
                    xtype:"polar",
                    //padding:'10 0 10 0',
                    //title:'规则类型错误数据占比扇形图',
                    reference: 'chart',
                    insetPadding: 10,
                    innerPadding: 10,
                    padding:5,
                    autoScroll:true,
                    height: 450,
                    plugins: {
                        ptype: 'chartitemevents',
                        moveEvents: true
                    },
                    //innerPadding: 5,
                    interactions: ['rotate'],
                    store:{
                        fields: ['ruleName', 'data'],
                        autoLoad: true,
                        sortOnLoad: true,
                        proxy: {
                            type: 'ajax',
                            url : 'data/business.json',
                            reader: {
                                type: 'json',
                                rootProperty: ''
                            }
                        }
                    },
                    sprites: [{
                        type: 'text',
                        text: '规则类型占比',
                        fontSize: 18,
                        width: 100,
                        height: 30,
                        x: 30, // the sprite x position
                        y: 30  // the sprite y position
                    }, {
                        type: 'text',
                        text: 'Data: IDC Predictions - 2017',
                        x: 12,
                        y: 325
                    }, {
                        type: 'text',
                        text: 'Source: Internet',
                        x: 12,
                        y: 345
                    }],
                    animate:true,
                    legend: {
                        position: 'right'
                    },
                    series:[{
                        type: 'pie',
                        autoScroll: true,
                        angleField: 'data',
                        //radiusField:'data',
                        label: {
                            field: 'ruleName',
                            display: 'data',
                            font:10,
                            calloutLine: {
                                length: 60,
                                width: 3
                                // specifying 'color' is also possible here
                            },
                            renderer: function (text, sprite, config, rendererData, index) {
                                var Data = rendererData.store.data.items;
                                var thisdata = Data[index];
                                var percent = ((parseFloat(thisdata.get('data') / rendererData.store.sum('data')) * 100.0).toFixed(2))
                                return text + "-"+percent+"%";
                            }
                        },
                        triggerAfterDraw:true,
                        donut:true,
                        // colors: [ "#a61120","#94ae0a"],
                        showInLegend: true,
                        // label:{
                        //     display:"name",
                        //     contrast:true,
                        // },
                        tooltip: {
                            trackMouse: true,
                            renderer: function (tooltip, record, item) {
                                tooltip.setHtml(record.get('ruleName') + ': ' + record.get('data'));
                            }
                        },
                        highlightCfg: {
                            margin: 10,
                            fillOpacity: .7
                        },
                        highlight:{
                            segment:{
                                margin:20
                            }
                        },
                        listeners:{
                            itemclick:function( series, item, event, eOpts){
                                // var rec=this.getStore().getAt(item.index);
                            }
                        }

                    }],
                    columnWidth:0.5
                },
                {
                    xtype: 'cartesian',
                    title:'规则类型错误数统计',
                    padding:5,
                    height: 450,
                    autoScroll: true,
                    store:{
                        //fields: ['ruleName', 'data'],
                        autoLoad: true,
                        sortOnLoad: true,
                        proxy: {
                            type: 'ajax',
                            url : 'data/testBar.json',
                            reader: {
                                type: 'json',
                                rootProperty: ''
                            }
                        }
                    },
                    axes: [{
                        type: 'numeric',
                        position: 'left',
                        title: {
                            text: '错误数',
                            fontSize: 15
                        },
                        fields: 'data'
                    }, {
                        type: 'category',
                        position: 'bottom',
                        title: {
                            text: '规则类型',
                            fontSize: 15
                        },
                        fields: 'ruleName'
                    }],
                    series: {
                        type: 'bar',
                        subStyle: {
                            fill: ['#388FAD'],
                            stroke: '#1F6D91'
                        },
                        xField: 'ruleName',
                        yField: 'data',
                        renderer:function(sprite, config, rendererData, index){

                        },
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('data');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.ruleName+':'+item.data.data)
                            }
                        }
                    },
                    columnWidth:0.5
                }
            ]
        }
    ]
});