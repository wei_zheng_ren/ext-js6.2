Ext.define('myapp.view.baobiao.SumBaoBiao', {
    extend: 'Ext.panel.Panel',
    xtype:'SumBaoBiao',
    title:"业务错误数据",
    layout :'fit',
    autoScroll: true,
    width:'100%',
    collapsible: true,
    height:900,
    items:[
        {
            xtype:'panel',
            width:'100%',
            height:'100%',
            layout: 'column',
            autoScroll: true,
            items: [
                {
                    xtype:"polar",
                    //padding:'10 0 10 0',
                    title:'业务准确性错误数据占比扇形图',
                    padding:5,
                    height: 450,
                    plugins: {
                        ptype: 'chartitemevents',
                        moveEvents: true
                    },
                    innerPadding: 5,
                    interactions: ['rotate', 'itemhighlight'],
                    store:{
                        fields: ['tableName', 'Errdata'],
                        autoLoad: true,
                        sortOnLoad: true,
                        proxy: {
                            type: 'ajax',
                            url : 'data/testdata.json',
                            reader: {
                                type: 'json',
                                rootProperty: 'zqx'
                            }
                        }
                    },
                    animate:true,
                    legend: {
                        position: 'right'
                    },
                    axes: [{
                        type: 'numeric',
                        position: 'radial',
                        fields: 'Errdata',
                        renderer:  function (axis, label, layoutContext) {
                            return layoutContext.renderer(label) + '%';
                        },
                        grid: true,
                        minimum: 0,
                        maximum: 2000000,
                        majorTickSteps: 4
                    }, {
                        type: 'category',
                        position: 'angular',
                        grid: true
                    }],
                    series:[{
                        type: 'radar',
                        autoScroll: true,
                        angleField: 'tableName',
                        radiusField: 'Errdata',
                        //radiusField:'data',
                        label: {
                            field: 'tableName',
                            display: 'rotate',
                            font:10,
                            renderer: function (text, sprite, config, rendererData, index) {
                                var Data = rendererData.store.data.items;
                                var thisdata = Data[index];
                                var percent = ((parseFloat(thisdata.get('Errdata') / rendererData.store.sum('Errdata')) * 100.0).toFixed(2))
                                return text + "-"+percent+"%";
                            }
                        },
                        triggerAfterDraw:true,
                        donut:true,
                        // colors: [ "#a61120","#94ae0a"],
                        showInLegend: true,
                        // label:{
                        //     display:"name",
                        //     contrast:true,
                        // },
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+":"+item.data.Errdata)
                            }
                        },
                        highlightCfg: {
                            margin: 10,
                            fillOpacity: .7
                        },
                        highlight:{
                            segment:{
                                margin:20
                            }
                        },
                        listeners:{
                            itemclick:function( series, item, event, eOpts){
                                // var rec=this.getStore().getAt(item.index);
                            }
                        }

                    }],
                    columnWidth:0.5
                },
                {
                    xtype: 'cartesian',
                    padding:5,
                    autoScroll: true,
                    title:'完整性柱形图',
                    // width: '100%',
                    height:450,
                    insetPadding: 40,
                    flipXY: true,
                    interactions: {
                        type: 'itemedit',
                        style: {
                            lineWidth: 2
                        },
                        tooltip: {
                            renderer:  function (tooltip, item, target, e) {
                                var formatString = '0,000 (billions of USD)',
                                    record = item.record;

                                tooltip.setHtml(record.get('country') + ': ' +
                                    Ext.util.Format.number(target.yValue, formatString));
                            }
                        }
                    },
                    animation: {
                        easing: 'easeOut',
                        duration: 500
                    },
                    store:{

                        fields: ['tableName', 'Errdata'],
                        autoLoad: true,
                        sortOnLoad: true,
                        proxy: {
                            type: 'ajax',
                            url : 'data/testdata.json',
                            reader: {
                                type: 'json',
                                rootProperty: 'zqx'
                            }
                        }
                    },
                    axes: [{
                        type: 'numeric',
                        position: 'bottom',
                        fields: 'Errdata',
                        grid: true,
                        maximum: 4000000,
                        majorTickSteps: 10,
                        title: 'Billions of USD',
                        renderer: function (axis, label, layoutContext) {
                            return Ext.util.Format.number(layoutContext.renderer(label) / 1000, '0,000');
                        }
                    }, {
                        type: 'category',
                        position: 'left',
                        fields: 'tableName',
                        grid: true
                    }],
                    series: [{
                        autoScroll: true,
                        type: 'bar',
                        xField: 'tableName',
                        yField: 'Errdata',
                        style: {
                            opacity: 0.80,
                            minGapWidth: 20
                        },
                        highlightCfg: {
                            strokeStyle: 'black',
                            radius: 20
                        },
                        label: {
                            field: 'Errdata',
                            display: 'insideEnd',
                            renderer: function (v) {
                                return Ext.util.Format.number(v / 1000, '0,000');

                            }
                            },
                        tooltip: {
                            trackMouse: true,
                            renderer: function(tooltip, record, item) {
                                var formatString = '0,000 (millions of USD)';

                                tooltip.setHtml(record.get('country') + ': ' +
                                    Ext.util.Format.number(record.get('ind'), formatString));
                            },
                        }
                    }],
                    sprites: [{
                        type: 'text',
                        text: 'Industry size in major economies (2011)',
                        fontSize: 22,
                        width: 100,
                        height: 30,
                        x: 40, // the sprite x position
                        y: 20  // the sprite y position
                    }],
                columnWidth:0.5
                },
                {
                    xtype:"polar",
                    //padding:'10 0 10 0',
                    title:'完整性扇形图',
                    padding:5,
                    height: 450,
                    plugins: {
                        ptype: 'chartitemevents',
                        moveEvents: true
                    },
                    innerPadding: 5,
                    interactions: ['rotate', 'itemhighlight'],
                    store:{
                        fields: ['tableName', 'Errdata'],
                        autoLoad: true,
                        sortOnLoad: true,
                        proxy: {
                            type: 'ajax',
                            url : 'data/testdata.json',
                            reader: {
                                type: 'json',
                                rootProperty: 'wzx'
                            }
                        }
                    },
                    animate:true,
                    legend: {
                        position: 'right'
                    },
                    series:[{
                        type: 'pie',
                        autoScroll: true,
                        angleField: 'Errdata',
                        //radiusField:'data',
                        label: {
                            field: 'tableName',
                            display: 'rotate',
                            font:10,
                            renderer: function (text, sprite, config, rendererData, index) {
                                var Data = rendererData.store.data.items;
                                var thisdata = Data[index];
                                var percent = ((parseFloat(thisdata.get('Errdata') / rendererData.store.sum('Errdata')) * 100.0).toFixed(2))
                                return text + "-"+percent+"%";
                            }
                        },
                        triggerAfterDraw:true,
                        donut:true,
                        // colors: [ "#a61120","#94ae0a"],
                        showInLegend: true,
                        // label:{
                        //     display:"name",
                        //     contrast:true,
                        // },
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+":"+item.data.Errdata)
                            }
                        },
                        highlightCfg: {
                            margin: 10,
                            fillOpacity: .7
                        },
                        highlight:{
                            segment:{
                                margin:20
                            }
                        },
                        listeners:{
                            itemclick:function( series, item, event, eOpts){
                                // var rec=this.getStore().getAt(item.index);
                            }
                        }

                    }],
                    columnWidth:0.5
                },
                {
                    xtype: 'cartesian',
                    title:'完整性柱形图',
                    padding:5,
                    height: 450,
                    autoScroll: true,
                    store:{
                        fields: ['tableName', 'Errdata'],
                        autoLoad: true,
                        sortOnLoad: true,
                        proxy: {
                            type: 'ajax',
                            url : 'data/testdata.json',
                            reader: {
                                type: 'json',
                                rootProperty: 'wzx'
                            }
                        }
                    },
                    axes: [{
                        type: 'numeric',
                        position: 'left',
                        title: {
                            text: '错误数',
                            fontSize: 15
                        },
                        fields: 'Errdata'
                    }, {
                        type: 'category',
                        position: 'bottom',
                        title: {
                            text: '业务字段',
                            fontSize: 15
                        },
                        fields: 'tableName'
                    }],
                    series: {
                        type: 'bar',
                        subStyle: {
                            fill: ['#388FAD'],
                            stroke: '#1F6D91'
                        },
                        xField: 'tableName',
                        yField: 'Errdata',
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+':'+item.data.Errdata)
                            }
                        }
                    },
                    columnWidth:0.5
                },
                {
                    xtype:"polar",
                    //padding:'10 0 10 0',
                    title:'及时性扇形图',
                    padding:5,
                    height: 450,
                    plugins: {
                        ptype: 'chartitemevents',
                        moveEvents: true
                    },
                    innerPadding: 5,
                    interactions: ['rotate', 'itemhighlight'],
                    store:{
                        fields: ['tableName', 'Errdata'],
                        autoLoad: true,
                        sortOnLoad: true,
                        proxy: {
                            type: 'ajax',
                            url : 'data/testdata.json',
                            reader: {
                                type: 'json',
                                rootProperty: 'jsx'
                            }
                        }
                    },
                    animate:true,
                    legend: {
                        position: 'right'
                    },
                    series:[{
                        type: 'pie',
                        autoScroll: true,
                        angleField: 'Errdata',
                        //radiusField:'data',
                        label: {
                            field: 'tableName',
                            display: 'rotate',
                            font:10,
                            renderer: function (text, sprite, config, rendererData, index) {
                                var Data = rendererData.store.data.items;
                                var thisdata = Data[index];
                                var percent = ((parseFloat(thisdata.get('Errdata') / rendererData.store.sum('Errdata')) * 100.0).toFixed(2))
                                return text + "-"+percent+"%";
                            }
                        },
                        triggerAfterDraw:true,
                        donut:true,
                        // colors: [ "#a61120","#94ae0a"],
                        showInLegend: true,
                        // label:{
                        //     display:"name",
                        //     contrast:true,
                        // },
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+":"+item.data.Errdata)
                            }
                        },
                        highlightCfg: {
                            margin: 10,
                            fillOpacity: .7
                        },
                        highlight:{
                            segment:{
                                margin:20
                            }
                        },
                        listeners:{
                            itemclick:function( series, item, event, eOpts){
                                // var rec=this.getStore().getAt(item.index);
                            }
                        }

                    }],
                    columnWidth:0.5
                },
                {
                    xtype: 'cartesian',
                    title:'及时性柱形图',
                    padding:5,
                    height: 450,
                    autoScroll: true,
                    store:{
                        fields: ['tableName', 'Errdata'],
                        autoLoad: true,
                        sortOnLoad: true,
                        proxy: {
                            type: 'ajax',
                            url : 'data/testdata.json',
                            reader: {
                                type: 'json',
                                rootProperty: 'jsx'
                            }
                        }
                    },
                    axes: [{
                        type: 'numeric',
                        position: 'left',
                        title: {
                            text: '错误数',
                            fontSize: 15
                        },
                        fields: 'Errdata'
                    }, {
                        type: 'category',
                        position: 'bottom',
                        title: {
                            text: '业务字段',
                            fontSize: 15
                        },
                        fields: 'tableName'
                    }],
                    series: {
                        type: 'bar',
                        subStyle: {
                            fill: ['#388FAD'],
                            stroke: '#1F6D91'
                        },
                        xField: 'tableName',
                        yField: 'Errdata',
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+':'+item.data.Errdata)
                            }
                        }
                    },
                    columnWidth:0.5
                },
                {
                    xtype:"polar",
                    //padding:'10 0 10 0',
                    title:'时效性扇形图',
                    padding:5,
                    height: 450,
                    plugins: {
                        ptype: 'chartitemevents',
                        moveEvents: true
                    },
                    innerPadding: 5,
                    interactions: ['rotate', 'itemhighlight'],
                    store:{
                        fields: ['tableName', 'Errdata'],
                        autoLoad: true,
                        sortOnLoad: true,
                        proxy: {
                            type: 'ajax',
                            url : 'data/testdata.json',
                            reader: {
                                type: 'json',
                                rootProperty: 'sxx'
                            }
                        }
                    },
                    animate:true,
                    legend: {
                        position: 'right'
                    },
                    series:[{
                        type: 'pie',
                        autoScroll: true,
                        angleField: 'Errdata',
                        //radiusField:'data',
                        label: {
                            field: 'tableName',
                            display: 'rotate',
                            font:10,
                            renderer: function (text, sprite, config, rendererData, index) {
                                var Data = rendererData.store.data.items;
                                var thisdata = Data[index];
                                var percent = ((parseFloat(thisdata.get('Errdata') / rendererData.store.sum('Errdata')) * 100.0).toFixed(2))
                                return text + "-"+percent+"%";
                            }
                        },
                        triggerAfterDraw:true,
                        donut:true,
                        // colors: [ "#a61120","#94ae0a"],
                        showInLegend: true,
                        // label:{
                        //     display:"name",
                        //     contrast:true,
                        // },
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+":"+item.data.Errdata)
                            }
                        },
                        highlightCfg: {
                            margin: 10,
                            fillOpacity: .7
                        },
                        highlight:{
                            segment:{
                                margin:20
                            }
                        },
                        listeners:{
                            itemclick:function( series, item, event, eOpts){
                                // var rec=this.getStore().getAt(item.index);
                            }
                        }

                    }],
                    columnWidth:0.5
                },
                {
                    xtype: 'cartesian',
                    title:'时效性柱形图',
                    padding:5,
                    height: 450,
                    autoScroll: true,
                    store:{
                        fields: ['tableName', 'Errdata'],
                        autoLoad: true,
                        sortOnLoad: true,
                        proxy: {
                            type: 'ajax',
                            url : 'data/testdata.json',
                            reader: {
                                type: 'json',
                                rootProperty: 'sxx'
                            }
                        }
                    },
                    axes: [{
                        type: 'numeric',
                        position: 'left',
                        title: {
                            text: '错误数',
                            fontSize: 15
                        },
                        fields: 'Errdata'
                    }, {
                        type: 'category',
                        position: 'bottom',
                        title: {
                            text: '业务字段',
                            fontSize: 15
                        },
                        fields: 'tableName'
                    }],
                    series: {
                        type: 'bar',
                        subStyle: {
                            fill: ['#388FAD'],
                            stroke: '#1F6D91'
                        },
                        xField: 'tableName',
                        yField: 'Errdata',
                        tips:{
                            trackMouse:true,
                            renderer:function(storeItem,item){
                                var total = 0;
                                this.getStore().each(function(rec) {
                                    total += rec.get('Errdata');
                                });
                                //设置贴士内容
                                storeItem.setTitle(item.data.tableName+':'+item.data.Errdata)
                            }
                        }
                    },
                    columnWidth:0.5
                }
            ]
        }
    ]
});