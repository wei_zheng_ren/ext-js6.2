Ext.define('myapp.view.baobiao.Main', {
    extend: 'Ext.panel.Panel',
    xtype: 'mainbaobiao',
    title:'aaa',
    scrollable : true,
    width: '100%',
    autoScroll: true,
    //layout:'border',
    minHeight:900,
    //titleCollapse: true,w
    height:'100%',
    items: [
        {
            xtype:'panel',
            layout: {
                type: 'fit',
            },
            width:'100%',
            height:'100%',
            overflowHandler:'scroller',
            autoScroll: true,
            //titleCollapse: true,
            items: [
                {
                    xtype:'panel',
                    title:'3.规则类型错误数总数',
                    width: '90%',
                    height:500,
                    hidden:true,   //这里隐藏
                },
                {
                    xtype:'RuleErrDataProportion',
                    title:'1.规则类型错误数总数',
                    // width:'90%',
                    padding:'5 0 5 0'
                },{
                    xtype:'SumBaoBiao',
                    title:'2.业务错误数据',
                    // width:'90%',
                    padding:'5 0 5 0'
                }
            ]
        }
    ]
});