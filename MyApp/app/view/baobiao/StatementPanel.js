Ext.define('myapp.view.baobiao.StatementPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'statementPanel',
    alias:'statementPanel',
    layout: {
        type: 'fit',
        // multi: true,
        // animate: true,
    },
    requires:[
        'myapp.view.baobiao.RuleTypeProportion'
    ],
    autoScroll: true,
    width: '100%',
    minHeight:900,
    height:'100%',
    items: [
        {
            xtype:'RuleTypeProportion',
            title:false,
            width:'90%',
            height:500,
            padding:'5 0 5 0',
            // collapsed: true,
            // hideCollapseTool: false
        }
        // {
        //     hidden:true,   //这里隐藏
        // },{
        //     xtype:'BusinessProportion',
        //     //hideCollapseTool: false,
        //     title:'2.业务错误数据',
        //     // width:'90%',
        //     //collapsed: true,
        //     padding:'5 0 5 0'
        // },
        // {
        //     hidden:true,   //这里隐藏
        // },{
        //     xtype:'HospitalProportion',
        //    // hideCollapseTool: false,
        //     title:'3.医院错误数比例',
        //     // width:'90%',
        //    // collapsed: true,
        //     padding:'5 0 5 0',
        //     listeners: {
        //         beforerender  :function(newActiveItem, mea, oldActiveItem, eOpts){
        //             var me=this;
        //             //获取当前账号信息
        //             var userInfo=Ext.ComponentQuery.query('mainbottom')[0].controller.getViewModel().get('userInfo');
        //             //如果当前账号为admin则隐藏该模块
        //             if(userInfo.account=='admin'){
        //                 me.hidden=false;
        //             }else{
        //                 me.hidden=true;
        //             }
        //         }
        //     }
        // },
        // {
        //     hidden:true,   //这里隐藏
        // }
    ]

});