Ext.define('myapp.view.baobiao.RuleTypeProportion', {
    extend: 'Ext.panel.Panel',
    xtype: 'RuleTypeProportion',
    layout: 'fit',
    //controller: 'system-quality-gauging-StatementPanelController',
    autoScroll: true,
    width: '90%',
    requires: [
        'myapp.view.baobiao.StatementPie'
    ],
    collapsible: true,
    height: 500,
    viewModel: {
        stores: {

            BusinessProportion: {
                fields: ['tableName', 'Errdata'],
                autoLoad: true,
                sortOnLoad: true,
                proxy: {
                    type: 'ajax',
                    url: '/data/testdata.json',
                    reader: {
                        type: 'json',
                        rootProperty: 'zqx'
                    }
                }
            },
            wzx: {
                fields: ['tableName', 'Errdata'],
                autoLoad: true,
                sortOnLoad: true,
                proxy: {
                    type: 'ajax',
                    url: '/data/testdata.json',
                    reader: {
                        type: 'json',
                        rootProperty: 'wzx'
                    }
                }
            },
            jsx: {
                fields: ['tableName', 'Errdata'],
                autoLoad: true,
                sortOnLoad: true,
                proxy: {
                    type: 'ajax',
                    url: '/data/testdata.json',
                    reader: {
                        type: 'json',
                        rootProperty: 'jsx'
                    }
                }
            },
            sxx: {
                fields: ['tableName', 'Errdata'],
                autoLoad: true,
                sortOnLoad: true,
                proxy: {
                    type: 'ajax',
                    url: '/data/testdata.json',
                    reader: {
                        type: 'json',
                        rootProperty: 'sxx'
                    }
                }
            }
        }
    },
    items: [
        {
            xtype: 'panel',
            title: "业务错误数据统计图",
            width: '90%',
            height: '100%',
            layout: 'column',
            autoScroll: true,
            items: [
                {
                    xtype:'system-quality-gauging-StatementPie',
                    params:{
                        xField: 'tableName',
                        yField: 'Errdata',
                        url:'/dataRepotForms/getBusinessProportion',
                        store:'{BusinessProportion}',
                        rootProperty:'BusinessProportion.zqx',
                        text:'准确性错误数扇形图'
                    },
                    columnWidth: 0.5
                },
                {
                    xtype: 'system-quality-gauging-hospitalProportionBar',
                    params:{
                        xField: 'tableName',
                        yField: 'Errdata',
                        url:'/dataRepotForms/getBusinessProportion',
                        rootProperty:'BusinessProportion',
                        text:'准确性错误数统计'
                    },
                    columnWidth: 0.5
                },
                {
                    xtype:'system-quality-gauging-StatementPie',
                    params:{
                        xField: 'tableName',
                        yField: 'Errdata',
                        url:'/dataRepotForms/getBusinessProportion',
                        store:'{wzx}',
                        rootProperty:'wzx',
                        text:'完整性错误数扇形图'
                    },
                    columnWidth: 0.5
                },
                {
                    xtype: 'system-quality-gauging-hospitalProportionBar',
                    params:{
                        xField: 'tableName',
                        yField: 'Errdata',
                        url:'/dataRepotForms/getBusinessProportion',
                        rootProperty:'wzx',
                        text:'完整性错误数统计'
                    },
                    columnWidth: 0.5
                },
                {
                    xtype:'system-quality-gauging-StatementPie',
                    params:{
                        xField: 'tableName',
                        yField: 'Errdata',
                        store:'{jsx}',
                        url:'/dataRepotForms/getBusinessProportion',
                        rootProperty:'jsx',
                        text:'及时性错误数扇形图'
                    },
                    columnWidth: 0.5
                },
                {
                    xtype: 'system-quality-gauging-hospitalProportionBar',
                    params:{
                        xField: 'tableName',
                        yField: 'Errdata',
                        url:'/dataRepotForms/getBusinessProportion',
                        rootProperty:'jsx',
                        text:'及时性错误数统计'
                    },
                    columnWidth: 0.5
                },
                {
                    xtype:'system-quality-gauging-StatementPie',
                    params:{
                        xField: 'tableName',
                        yField: 'Errdata',
                        store:'{sxx}',
                        url:'/dataRepotForms/getBusinessProportion',
                        rootProperty:'sxx',
                        text:'时效性错误数扇形图'
                    },
                    columnWidth: 0.5
                },
                {
                    xtype: 'system-quality-gauging-hospitalProportionBar',
                    params:{
                        xField: 'tableName',
                        yField: 'Errdata',
                        url:'/dataRepotForms/getBusinessProportion',
                        rootProperty:'sxx',
                        text:'时效性错误数统计'
                    },
                    columnWidth: 0.5
                }
                //##################################医院错误数占比

            ]
        }
    ]
});