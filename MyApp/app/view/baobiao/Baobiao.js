Ext.define('myapp.view.baobiao.Baobiao', {
    extend: 'Ext.panel.Panel',
    xtype:'Baobiao',
    title:"整体统计",
    layout : {
        type :'table',
        columns : 4,
        tableAttrs: {
            style: {
                width: '100%'
            }
        }
    },
    autoScroll: true,
    height:1000,
    items:[
        {
        xtype:"polar",
        width: 400,
        //padding:'10 0 10 0',
        title:'准确性',
        height: 500,
        plugins: {
            ptype: 'chartitemevents',
            moveEvents: true
        },
        innerPadding: 5,
        interactions: ['rotate', 'itemhighlight'],
        store:{
            fields: ['tableName', 'Errdata'],
            autoLoad: true,
            sortOnLoad: true,
            proxy: {
                type: 'ajax',
                url : 'data/testdata.json',
                reader: {
                    type: 'json',
                    rootProperty: 'zqx'
                }
            }
        },
        animate:true,
        legend: {
            position: 'right'
        },
        series:[{
            type: 'pie',
            title:'准确性',
            angleField: 'Errdata',
            //radiusField:'data',
            label: {
                field: 'tableName',
                display: 'rotate',
                font:10,
                renderer: function (text, sprite, config, rendererData, index) {
                    var Data = rendererData.store.data.items;
                    var thisdata = Data[index];
                    var percent = ((parseFloat(thisdata.get('Errdata') / rendererData.store.sum('Errdata')) * 100.0).toFixed(2))
                    return text + "-"+percent+"%";
                    //return rendererData.store.data.items[index].data.type+':'+rendererData.store.data.items[index].data.data
                }
            },
            donut:true,
           // colors: [ "#a61120","#94ae0a"],
                showInLegend: true,
            // label:{
            //     display:"name",
            //     contrast:true,
            // },
            tips:{
                trackMouse:true,
                renderer:function(storeItem,item){
                    var total = 0;
                    this.getStore().each(function(rec) {
                        total += rec.get('Errdata');
                    });
                    //设置贴士内容
                    storeItem.setTitle(item.data.tableName+":"+item.data.Errdata)
                }
            },
            highlightCfg: {
                margin: 10,
                fillOpacity: .7
            },
            highlight:{
                segment:{
                    margin:20
                }
            },
            listeners:{
                itemclick:function( series, item, event, eOpts){
                   // var rec=this.getStore().getAt(item.index);
                }
            }

        }]
    },
        {
        xtype:"polar",
        width: 400,
        height: 500,
        title:'完整性',

        plugins: {
            ptype: 'chartitemevents',
            moveEvents: true
        },
        innerPadding: 5,
        interactions: ['rotate', 'itemhighlight'],
        store:{
            fields: ['name', 'data'],
            autoLoad: true,
            sortOnLoad: true,
            proxy: {
                type: 'ajax',
                url : 'data/business.json',
                reader: {
                    type: 'json',
                    rootProperty: 'wzx'
                }
            }
        },
        animate:true,
        legend: {
            position: 'right'
        },
        series:[{
            type: 'pie',
            title:'准确性',
            angleField: 'data',
            //radiusField:'data',
            label: {
                field: 'name',
                display: 'rotate',
                font:10,
                renderer: function (text, sprite, config, rendererData, index) {
                    return ''+rendererData.store.data.items[index].data.data
                }
            },
            donut:true,
            colors: [ "#a61120","#94ae0a"],
            showInLegend: true,
            // label:{
            //     display:"name",
            //     contrast:true,
            // },
            tips:{
                trackMouse:true,
                renderer:function(storeItem,item){
                    var total = 0;
                    this.getStore().each(function(rec) {
                        total += rec.get('data');
                    });
                    //设置贴士内容
                    storeItem.setTitle(item.data.name+":"+item.data.data)
                }
            },
            highlightCfg: {
                margin: 10,
                fillOpacity: .7
            },
            highlight:{
                segment:{
                    margin:20
                }
            },
            listeners:{
                itemclick:function( series, item, event, eOpts){
                    var rec=this.getStore().getAt(item.index);
                }
            }

        }]
    },
        {
        xtype:"polar",
        width: 400,
        title:'及时性',
        height: 500,
        plugins: {
            ptype: 'chartitemevents',
            moveEvents: true
        },
        innerPadding: 5,
        interactions: ['rotate', 'itemhighlight'],
        store:{
            fields: ['name', 'data'],
            autoLoad: true,
            sortOnLoad: true,
            proxy: {
                type: 'ajax',
                url : 'data/business.json',
                reader: {
                    type: 'json',
                    rootProperty: 'jsx'
                }
            }
        },
        animate:true,
        legend: {
            position: 'right'
        },
        series:[{
            type: 'pie',
            title:'准确性',
            angleField: 'data',
            //radiusField:'data',
            label: {
                field: 'name',
                display: 'rotate',
                font:10,
                renderer: function (text, sprite, config, rendererData, index) {
                    return ''+rendererData.store.data.items[index].data.data
                }
            },
            donut:true,
            colors: [ "#a61120","#94ae0a"],
            showInLegend: true,
            // label:{
            //     display:"name",
            //     contrast:true,
            // },
            tips:{
                trackMouse:true,
                renderer:function(storeItem,item){
                    var total = 0;
                    this.getStore().each(function(rec) {
                        total += rec.get('data');
                    });
                    //设置贴士内容
                    storeItem.setTitle(item.data.name+":"+item.data.data)
                }
            },
            highlightCfg: {
                margin: 10,
                fillOpacity: .7
            },
            highlight:{
                segment:{
                    margin:20
                }
            },
            listeners:{
                itemclick:function( series, item, event, eOpts){
                    var rec=this.getStore().getAt(item.index);
                }
            }

        }]
    },
        {
        xtype:"polar",
        title:'时效性',
        width: 400,
        height: 500,
        plugins: {
            ptype: 'chartitemevents',
            moveEvents: true
        },
        innerPadding: 5,
        interactions: ['rotate', 'itemhighlight'],
        store:{
            fields: ['name', 'data'],
            autoLoad: true,
            sortOnLoad: true,
            proxy: {
                type: 'ajax',
                url : 'data/business.json',
                reader: {
                    type: 'json',
                    rootProperty: 'sxx'
                }
            }
        },
        animate:true,
        legend: {
            position: 'right'
        },
        series:[{
            type: 'pie',
            title:'准确性',
            angleField: 'data',
            //radiusField:'data',
            label: {
                field: 'name',
                display: 'rotate',
                font:10,
                renderer: function (text, sprite, config, rendererData, index) {
                    return ''+rendererData.store.data.items[index].data.data
                }
            },
            donut:true,
            colors: [ "#a61120","#94ae0a"],
            showInLegend: true,
            // label:{
            //     display:"name",
            //     contrast:true,
            // },
            tips:{
                trackMouse:true,
                renderer:function(storeItem,item){
                    var total = 0;
                    this.getStore().each(function(rec) {
                        total += rec.get('data');
                    });
                    //设置贴士内容
                    storeItem.setTitle(item.data.name+":"+item.data.data)
                }
            },
            highlightCfg: {
                margin: 10,
                fillOpacity: .7
            },
            highlight:{
                segment:{
                    margin:20
                }
            },
            listeners:{
                itemclick:function( series, item, event, eOpts){
                    var rec=this.getStore().getAt(item.index);
                }
            }

        }]
    },{
        xtype: 'cartesian',
        width: 600,
        height: 400,
        innerPadding: '0 10 0 10',
        store: {
            autoLoad: true,
            sortOnLoad: true,
            fields: ['ruleName', 'Sum','ErrSum'],
            proxy: {
                type: 'ajax',
                url : 'data/business.json',
                reader: {
                    type: 'json',
                    rootProperty: 'sum'
                }
            }
        },
        axes: [{
            type: 'numeric3d',
            position: 'left',
            fields: ['Sum', 'ErrSum'],
            title: {
                text: '数量',
                fontSize: 15
            },
            grid: {
                odd: {
                    fillStyle: 'rgba(255, 255, 255, 0.06)',
                    opacity: 1,
                    fill: '#ddd',
                    stroke: '#bbb',

                },
                even: {
                    fillStyle: 'rgba(0, 0, 0, 0.03)'
                }
            }
        }, {
            type: 'category3d',
            position: 'bottom',
            title: {
                text: '规则',
                fontSize: 15
            },
            fields: 'ruleName'
        }],
        series: {
            type: 'bar3d',
            xField: 'ruleName',
            yField: ['Sum', 'ErrSum'],
            label: {
                field: ['Sum', 'ErrSum'],
                display: 'insideEnd'
                //renderer: 'onSeriesLabelRender'
            },
            colors: [ "#94ae0a","#a61120", "#115fa6", "#ff8809", "#ffd13e", "#a61187", "#24ad9a", "#7c7474", "#a66111"],
            tips:{
                trackMouse:true,
                renderer:function(storeItem,item){
                    var total = 0;
                    this.getStore().each(function(rec) {
                        total += rec.get('data');
                    });
                    //设置贴士内容
                    storeItem.setTitle("错误数:"+item.data.ErrSum+'\n'+"正确数:"+item.data.Sum)
                }
            }
        }
    }]
});