Ext.define('myapp.view.Tree.CheckStore', {
    extend: 'Ext.data.TreeStore',
    alias: 'store.checktree',

    proxy: {
        type: 'ajax',
        url: 'data/data.json'
    },
    sorters: [{
        property: 'leaf',
        direction: 'ASC'
    }, {
        property: 'text',
        direction: 'ASC'
    }]
});