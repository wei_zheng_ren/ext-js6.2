Ext.define('myapp.view.Tree.CheckTreeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.check-tree',

    onBeforeCheckChange: function(record, checkedState, e) {
        
    },
    onCollapse:function(table, td, cellIndex, record, tr, rowIndex, e, eOpts){
        var list=[]
        var checkarr=[];
        if(record.data.children==null){
            list = record.parentNode.data;//父节点
        }else{
            list=record.data;
        }
        for(var i=0;i<list.children.length;i++){
            if(list.children[i].checked==true){
                checkarr.push(list.children[i]);
            }
        }
        if(checkarr.length>0){
            record.set('checked',true);
        }else{
            record.set('checked',false);
        }

        
    },
    onCheckedNodesClick: function() {
        var records = this.getView().getChecked(),
            names = [];

        Ext.Array.each(records, function(rec){
            names.push(rec.get('text'));
        });

        Ext.MessageBox.show({
            title: 'Selected Nodes',
            msg: names.join('<br />'),
            icon: Ext.MessageBox.INFO
        });
    }
});