Ext.define('myapp.view.Tree.CheckTree', {
    extend: 'Ext.tree.Panel',
    xtype: 'check-tree',
    checkPropagation: 'both',
    controller: 'check-tree',
    store: {
        proxy: {
            type: 'ajax',
            url:'data/data.json'
        }
    },
    rootVisible: false,
    useArrows: true,
    frame: true,
    title: 'Check Tree',
    width: 280,
    height: 300,
    bufferedRenderer: false,
    animate: true,
    listeners: {
        // checkchange: 'onCheckChange',
         cellclick:'onCollapse',
        //beforecheckchange: 'onBeforeCheckChange'
    },
    tbar: [{
        text: 'Get checked nodes',
        handler: 'onCheckedNodesClick'
    }]
});