Ext.define('myapp.view.panelandtabl.TestPanel', {
    extend: 'Ext.container.Container',
    xtype:'testPanel',
    layout: {
        type: 'hbox'
    },
    width: '100%',
    height:'100%',
    autoScroll:true,
    border: 1,
    viewModel:{
        stores:{
            business:{
                autoLoad: true,
                sortOnLoad: true,
                fields: ['tableName', 'statistic'],
                proxy: {
                    type: 'ajax',
                    url : 'data/business.json',
                    reader: {
                        type: 'json',
                        rootProperty: ''
                    }
                }
            }
        }
    },
    items:[
        {
            xtype:'panel',
            title:'业务统计报表',
            width: '100%',
            header:{
                cls:'x-panel-header-new'//关键代码
            },
            defaults: {
                collapsed: true,
                padding: '10 0 0 0',
                bodyStyle: {
                    background: '#ffc',
                    padding: '10px'
                }
            },
            autoScroll:true,
            layout: {
                type: 'vbox',
            },
            items: [
                {
                    title: '规则说明',
                    width: '100%',
                    plain: true,
                    height:500,
                    collapsible: true,
                    html:'规则说明'
                },
                {
                    xtype: 'panelandtabl-businesslistpanel',
                    title: '数据质量',
                    plain: true,
                    width: '100%',
                    height:500,
                    collapsible: true,
                    // itemSelector:'div.dataview-multisort-item',
                   bind:{
                        store:'{business}'
                   }

                },
                {
                    title: '报表',
                    plain: true,
                    width: '100%',
                    height:500,
                    collapsible: true,
                    html:'报表'
                },
                {
                    title: '安师大',
                    width: '100%',
                    height:500,
                    collapsible: true,
                    html:'安师大'
                },
                {
                    title: 'Collapsible',
                    width: '100%',
                    height:500,
                    collapsible: true,
                    html:'Collapsible'
                }
            ]
        }
    ]
})